<!DOCTYPE HTML>
<html lang="de-DE">
<head>
	<meta charset="UTF-8">
	<title>Interner Bereich</title>

	<link rel="stylesheet" type="text/css" href="./../css/intern/index2.css">
	
</head>
<body>

<?PHP

@session_start();
$sessionID = session_id();

@include("./../config/db_connect.php");
@include("./../functions/is_logged_in_for_chat.php");

$uID = $_SESSION["uID"];
$username = $_SESSION["username"];
$firstname = $_SESSION["firstname"];
$admin = $_SESSION["admin"];

$remoteADDR = $_SERVER["REMOTE_ADDR"];

$is_logged_in = is_logged_in($pdo, $sessionID);

if($is_logged_in == 1){
	echo "<div id='form'>";
	echo "<span>Hallo $firstname, <a href='./logout.php'>logout</a></span><hr/>";

	echo "<form action='./chatrooms_proceed.php' method='POST'>";
	
	echo "<h1>Chatroom erstellen</h1>";
	echo "Name: <input type='text' name='chatroom' required><br/>";
	echo "Passwort: <input type='password' name='password' required><br/>";
	echo "<input type='submit' value=' speichern '>";
	
	echo "</form>";
	
	echo "<hr/>";
	echo "<h1>Deine Chatrooms...</h1>";
	
	$stmt0 = $pdo->prepare("SELECT crNAME FROM doit_chatrooms WHERE `aID` = :bp_aID");
	$stmt0->bindParam(':bp_aID', $uID);
	$result0 = $stmt0->execute();
	$ergs0 = $stmt0->rowCount();
	
	for($i = 0; $i < $ergs0; $i++){
		$result0 = $stmt0->fetch(PDO::FETCH_OBJ);
		$crNAME = $result0->crNAME;
	  
		echo "" . $crNAME . "<br/>";
	}
	
	echo "</div>";
} else{
	@Header("Location: ./../login.php");
}

?>

</body>
</html>