<!DOCTYPE HTML>
<html lang="de-DE">
<head>
	<meta charset="UTF-8">
	<title>Interner Bereich</title>

	<link rel="stylesheet" type="text/css" href="./../css/intern/index2.css">
	
</head>
<body>

<?PHP

@session_start();
$sessionID = session_id();

@include("./../config/db_connect.php");
@include("./../functions/is_logged_in_for_chat.php");

$uID = $_SESSION["uID"];
$username = $_SESSION["username"];
$firstname = $_SESSION["firstname"];
$admin = $_SESSION["admin"];

$remoteADDR = $_SERVER["REMOTE_ADDR"];

$is_logged_in = is_logged_in($pdo, $sessionID);

if($is_logged_in == 1){
	echo "<div id='form'>";
	echo "<span>Hallo $firstname, <a href='./logout.php'>logout</a></span><hr/>";

	$fopen = fopen("./../css/chat/colors/colors_" . $uID . ".txt", "r");
	
	$counter = 0;
	
	$array = array();
	
	# /*
	while($fgets = fgets($fopen, 100000)){
		// echo "" . $fgets . "<br/>";
		$explode = explode("|", $fgets);		
		$array[$counter] = $explode[1];
		
		$counter++;
	}
	# /*
	
	echo "<form action='./colorsettings_proceed.php' method='POST'>";
	
	echo "<h1>Colorsettings</h1>";
	echo "Hintergrundfarbe: <input type='text' name='background' value='" . $array[0] . "'><br/>";
	echo "Chatfenster: <input type='text' name='chatwindow' value='" . $array[1] . "'><br/>";
	echo "Pers&ouml;nliche Farbe: <input type='text' name='pcolor' value='" . $array[2] . "'><br/>";
	echo "Wer ist online: <input type='text' name='whoisonline' value='" . $array[3] . "'><br/>";
	echo "Benutzer einf&auml;rben: <input type='text' name='highlight_user' value='" . $array[4] . "'><br/>";
	
	echo "<input type='submit' value=' speichern '>";
	
	echo "</form>";
	#*/
	echo "</div>";
} else{
	@Header("Location: ./../login.php");
}

?>

</body>
</html>