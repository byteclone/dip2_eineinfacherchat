<?PHP

@session_start();

@include("./../config/db_connect.php");
@include("./../functions/is_logged_in.php");

$uID = $_SESSION["uID"];
$username = $_SESSION["username"];
$firstname = $_SESSION["firstname"];
$admin = $_SESSION["admin"];

$remoteADDR = $_SERVER["REMOTE_ADDR"];

$is_logged_in = is_logged_in($pdo, $uID, $remoteADDR);

if($is_logged_in == 1){
	$bID = $_GET["bID"];

	$stmt1 = $pdo->prepare("SELECT uID FROM doit_userblacklist WHERE `blackID` = :bp_uID");
	$stmt1->bindParam(':bp_uID', $bID);
	$result1 = $stmt1->execute();
	$ergs1 = $stmt1->rowCount();
	
	// echo "" . $ergs1 . "<hr/>";
	
	if($ergs1 == 0){
		if($uID != $bID){	
			$stmt2 = $pdo->prepare("INSERT INTO doit_userblacklist SET `uID` = :bp_uID, `blackID` = :bp_blackID");
			$stmt2->bindParam(':bp_uID', $uID);
			$stmt2->bindParam(':bp_blackID', $bID);
			$result2 = $stmt2->execute();
			$ergs2 = $stmt2->rowCount();
		}
		
		// echo "" . $ergs2 . "";
	}
}

@Header("Location: ./blacklist.php");

?>