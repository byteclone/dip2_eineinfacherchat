
<?PHP

@session_start();
$sessionID = session_id();

@include("./../config/db_connect.php");
@include("./../functions/is_logged_in_for_chat.php");

$uID = $_SESSION["uID"];
$username = $_SESSION["username"];
$firstname = $_SESSION["firstname"];
$admin = $_SESSION["admin"];

$remoteADDR = $_SERVER["REMOTE_ADDR"];

$is_logged_in = is_logged_in($pdo, $sessionID);

if($is_logged_in == 1){

	$background = $_POST["background"];
	$chatwindow = $_POST["chatwindow"];
	$pcolor = $_POST["pcolor"];
	$whoisonline = $_POST["whoisonline"];
	$highlight_user = $_POST["highlight_user"];
	
	$text = "background|" . $background . "\n";
	$text .= "chatwindow|" . $chatwindow . "\n";
	$text .= "pcolor|" . $pcolor . "\n";
	$text .= "whoisonline|" . $whoisonline . "\n";
	$text .= "highlight_user|" . $highlight_user . "\n";
	
	file_put_contents("./../css/chat/colors/colors_" . $uID . ".txt", $text);

} else{
	@Header("Location: ./../login.php");
}

@Header("Location: ./colorsettings.php");

?>
