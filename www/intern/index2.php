<!DOCTYPE HTML>
<html lang="de-DE">
<head>
	<meta charset="UTF-8">
	<title>Interner Bereich</title>

	<link rel="stylesheet" type="text/css" href="./../css/intern/index2.css">
	
</head>
<body>

<?PHP

@session_start();
$sessionID = session_id();

@include("./../config/db_connect.php");
@include("./../functions/is_logged_in_for_chat.php");

$uID = $_SESSION["uID"];
$username = $_SESSION["username"];
$firstname = $_SESSION["firstname"];
$admin = $_SESSION["admin"];

$remoteADDR = $_SERVER["REMOTE_ADDR"];

$is_logged_in = is_logged_in($pdo, $sessionID);

if($is_logged_in == 1){
	echo "<div id='form'>";
	echo "<span>Hallo $firstname, <a href='./logout.php'>logout</a></span><hr/>";
	
	if($admin == 1){
		echo "<h1>Du bist Admin!</h1>";
		echo "<ul>";
		echo "<li><a href='./../admin_321q/index.php'>Benutzer aufnehmen/ablehnen...</a></li>";
		echo "<li><a href='./../admin_321q/settings.php'>Benutzereinstellungen festlegen...</a></li>";
		echo "</ul>";
	} else{
		echo "<span><i>Hallo Benutzer. Schade, du hast keine Adminrechte.<br/><br/>Aber herzlichen Glückwunsch, du bist drinn!</i></span>";
	}

	$stmt1 = $pdo->prepare("SELECT colorsettings, chatrooms, blacklist FROM doit_users WHERE `uID` = :bp_uID");
	$stmt1->bindParam(':bp_uID', $uID);
	$result1 = $stmt1->execute();
	
	$result1 = $stmt1->fetch(PDO::FETCH_OBJ);
	$colorsettings = $result1->colorsettings;
	$chatrooms = $result1->chatrooms;
	$blacklist = $result1->blacklist;
	
	if($colorsettings == 1){
		echo "<br/><a href='./colorsettings.php'>Chat Farben festlegen</a>";
	} else{
		echo "<br/>Chat Farben festlegen";
	}
	
	if($chatrooms == 1){
		echo "<br/><a href='./chatrooms.php'>Chatroom erstellen</a>";
	} else{
		echo "<br/>Chatroom erstellen";
	}
	
	if($blacklist == 1){
		echo "<br/><a href='./blacklist.php'>User Blacklist erstellen</a>";
	} else{
		echo "<br/>User Blacklist erstellen";
	}
	
	echo "</div>";
} else{
	@Header("Location: ./../login.php");
}

?>

</body>
</html>