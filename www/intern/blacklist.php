<!DOCTYPE HTML>
<html lang="de-DE">
<head>
	<meta charset="UTF-8">
	<title>Interner Bereich</title>

	<link rel="stylesheet" type="text/css" href="./../css/intern/index2.css">
		<script>
		function showHint(str)
		{
			if (str.length==0) { 
				document.getElementById("txtHint").innerHTML="";
				return;
			} else {
				var xmlhttp=new XMLHttpRequest();
				xmlhttp.onreadystatechange=function() {
					if (xmlhttp.readyState==4 && xmlhttp.status==200) {
						document.getElementById("txtHint").innerHTML=xmlhttp.responseText;
					}
				}
				xmlhttp.open("GET","./dbtest.php?q="+str,true);
				xmlhttp.send();
			}    
		}
		</script>	
</head>
<body>

<?PHP

@session_start();

@include("./../config/db_connect.php");
@include("./../functions/is_logged_in.php");

$uID = $_SESSION["uID"];
$username = $_SESSION["username"];
$firstname = $_SESSION["firstname"];
$admin = $_SESSION["admin"];

$remoteADDR = $_SERVER["REMOTE_ADDR"];

$is_logged_in = is_logged_in($pdo, $uID, $remoteADDR);

if($is_logged_in == 1){
	echo "<div id='form'>";
	echo "<span>Hallo $firstname, <a href='./logout.php'>logout</a></span><hr/>";

	echo "<h1>Erstellen einer Blacklist...</h1>";

?>	
	
	<p><b>Einfach drauf los schreiben, System erkennt Benutzernamen:</b></p>
		<form action=''>
			Benutzername: <input type='text' id='txt1' onkeyup='showHint(this.value)'>
		</form>
	<p>Vorschl&auml;ge: <span id='txtHint'></span></p>

<?PHP	
	
	$stmt1 = $pdo->prepare("SELECT blackID FROM doit_userblacklist WHERE `uID` = :bp_uID");
	$stmt1->bindParam(':bp_uID', $uID);
	$result1 = $stmt1->execute();
	$ergs1 = $stmt1->rowCount();
	
	// echo "<hr/>" . $ergs1 . "<hr/>";
	
	if($ergs1 > 0){	
		echo "Auf deiner Blacklist stehen...<hr/>";
	
		for($i = 0; $i < $ergs1; $i++){
			$result1 = $stmt1->fetch(PDO::FETCH_OBJ);
			$blackID = $result1->blackID;
		
			$stmt2 = $pdo->prepare("SELECT username FROM doit_users WHERE `uID` = :bp_blackID");
			$stmt2->bindParam(':bp_blackID', $blackID);
			$result2 = $stmt2->execute();
			$ergs2 = $stmt2->rowCount();
			
			$result2 = $stmt2->fetch(PDO::FETCH_OBJ);
			$blacklist = $result2->username;

			echo "<a href='./blacklist_eraseuser.php?blackID=" . $blackID . "'>" . $blacklist . "</a><br/>";
		}
	}

	echo "</div>";
} else{
	@Header("Location: ./../login.php");
}

?>

</body>
</html>