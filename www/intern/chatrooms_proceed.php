<?PHP

@session_start();
$sessionID = session_id();

@include("./../config/db_connect.php");
@include("./../functions/is_logged_in_for_chat.php");

$uID = $_SESSION["uID"];
$username = $_SESSION["username"];
$firstname = $_SESSION["firstname"];
$admin = $_SESSION["admin"];

$remoteADDR = $_SERVER["REMOTE_ADDR"];

$is_logged_in = is_logged_in($pdo, $sessionID);

if($is_logged_in == 1){

	$crNAME = $_POST["chatroom"];
	$crPW = $_POST["password"];
	
	$pw_sha1 = SHA1($crPW);
	
	$stmt1 = $pdo->prepare("SELECT * FROM doit_chatrooms WHERE `crNAME` = :bp_crNAME");
	$stmt1->bindParam(':bp_crNAME', $crNAME);
	$result1 = $stmt1->execute();
	$ergs1 = $stmt1->rowCount();
	
	if($ergs1 == 0){	
		// hole den zeitstempel des logins vom aktuellen benutzer...
		$stmt0 = $pdo->prepare("INSERT INTO doit_chatrooms SET `aID` = :bp_aID, `crNAME` = :bp_name, `crPASSWORD` = :bp_password, `status` = 1");
		$stmt0->bindParam(':bp_aID', $uID);
		$stmt0->bindParam(':bp_name', $crNAME);
		$stmt0->bindParam(':bp_password', $pw_sha1);
		$result0 = $stmt0->execute();
		
		@Header("Location: ./chatrooms.php");
	} else{
		echo "Der Chatroom mit dem Namen " . $crNAME . " existiert bereits!<br/>Bitte w&auml;hle einen anderen Namen aus.<br/><br/><a href='./chatrooms.php'>zur&uuml;ck</a>";
	}
} else{
	@Header("Location: ./../login.php");
}

?>