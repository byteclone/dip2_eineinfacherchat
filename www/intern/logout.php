<?PHP
@session_start();
$sessionID = @session_id();

@include("./../config/db_connect.php");
@include("./../functions/is_logged_in.php");

$uID = $_SESSION["uID"];
$remoteADDR = $_SERVER["REMOTE_ADDR"];

$is_logged_in = 0;
$is_logged_in = is_logged_in($pdo, $uID, $remoteADDR);
	
if($is_logged_in == 1){	
	$stmt1 = $pdo->prepare("DELETE FROM doit_sessions WHERE `uID` = :bp_uID AND `sessionID` = :bp_sessionID AND `remoteADDR` = :bp_remoteADDR");
	$stmt1->bindParam(':bp_uID', $uID);
	$stmt1->bindParam(':bp_sessionID', $sessionID);
	$stmt1->bindParam(':bp_remoteADDR', $remoteADDR);
	$result1 = $stmt1->execute();
		
	// http://php.net/manual/de/function.session-destroy.php
	if (ini_get("session.use_cookies")) {
		$params = session_get_cookie_params();
		setcookie(session_name(), '', time() - 42000, $params["path"],
			$params["domain"], $params["secure"], $params["httponly"]
		);
	}

	@session_destroy();

	@Header("Location: ./../index.html");
}

?>