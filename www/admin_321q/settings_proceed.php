<?PHP
@session_start();
$sessionID = session_id();

@include("./../config/db_connect.php");
@include("./../functions/is_logged_in_for_chat.php");

$uID = $_SESSION["uID"];
$username = $_SESSION["username"];
$firstname = $_SESSION["firstname"];
$admin = $_SESSION["admin"];

$remoteADDR = $_SERVER["REMOTE_ADDR"];

$is_logged_in = is_logged_in($pdo, $sessionID);

if(($is_logged_in == 1) && ($admin == 1)){

	$settingsID = $_POST["settingsID"];
	$colorsettings = $_POST["colorsettings"];	
	$chatrooms = $_POST["chatrooms"];
	$blacklist = $_POST["blacklist"];
	
	if($colorsettings == "on"){
		$colorsettings = 1;
	}
	
	if($chatrooms == "on"){
		$chatrooms = 1;
	}
	
	if($blacklist == "on"){
		$blacklist = 1;
	}
	
	var_dump($settingsID);
	var_dump($colorsettings);
	var_dump($chatrooms);
	var_dump($blacklist);
	
	$stmt1 = $pdo->prepare("UPDATE doit_users SET `colorsettings` = :bp_colorsettings, `chatrooms` = :bp_chatrooms, `blacklist` = :bp_blacklist WHERE `uID` = :bp_settingsID");
	$stmt1->bindParam(':bp_settingsID', $settingsID);
	$stmt1->bindParam(':bp_colorsettings', $colorsettings);
	$stmt1->bindParam(':bp_chatrooms', $chatrooms);
	$stmt1->bindParam(':bp_blacklist', $blacklist);
	$result1 = $stmt1->execute();

}

@Header("Location: ./settings.php");

?>
