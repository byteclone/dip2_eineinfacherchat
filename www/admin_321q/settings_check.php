<?PHP
@session_start();
$sessionID = session_id();

@include("./../config/db_connect.php");
@include("./../functions/is_logged_in_for_chat.php");

$uID = $_SESSION["uID"];
$username = $_SESSION["username"];
$firstname = $_SESSION["firstname"];
$admin = $_SESSION["admin"];

$remoteADDR = $_SERVER["REMOTE_ADDR"];

$is_logged_in = is_logged_in($pdo, $sessionID);

if(($is_logged_in == 1) && ($admin == 1)){

	$settingsID = $_GET["settingsID"];
	
	$stmt1 = $pdo->prepare("SELECT username, colorsettings, chatrooms, blacklist FROM doit_users WHERE `uID` = :bp_settingsID");
	$stmt1->bindParam(':bp_settingsID', $settingsID);
	$result1 = $stmt1->execute();
	$ergs1 = $stmt1->rowCount();
	
	if($ergs1 == 1){
		$result1 = $stmt1->fetch(PDO::FETCH_OBJ);
		$username = $result1->username;
		$colorsettings = $result1->colorsettings;
		$chatrooms = $result1->chatrooms;
		$blacklist = $result1->blacklist;

		echo "<form action='./settings_proceed.php' method='POST'>";
			echo "" . $username . "&nbsp;&nbsp;&nbsp;";
			echo "<input type='hidden' name='settingsID' value='" . $settingsID . "'>";
			
			if($colorsettings == 1){			
				echo "<input type='checkbox' name='colorsettings' value='on' checked>&nbsp;&nbsp;&nbsp;";
			} else{
				echo "<input type='checkbox' name='colorsettings' value='on'>&nbsp;&nbsp;&nbsp;";
			}
			
			if($chatrooms == 1){
				echo "<input type='checkbox' name='chatrooms' value='on' checked>&nbsp;&nbsp;&nbsp;";
			} else{
				echo "<input type='checkbox' name='chatrooms' value='on'>&nbsp;&nbsp;&nbsp;";
			}
			
			if($blacklist == 1){
				echo "<input type='checkbox' name='blacklist' value='on' checked>&nbsp;&nbsp;&nbsp;";
			} else{
				echo "<input type='checkbox' name='blacklist' value='on'>&nbsp;&nbsp;&nbsp;";
			}
			echo "<input type='submit' value=' speichern '>";
		echo "</form>";
	}
}

?>
