<!DOCTYPE HTML>
<html lang="de-DE">
<head>
	<meta charset="UTF-8">
	<title>Interner Bereich</title>

	<link rel="stylesheet" type="text/css" href="./../css/intern/index2.css">
		<script>
		function showHint(str)
		{
			if (str.length==0) { 
				document.getElementById("txtHint").innerHTML="";
				return;
			} else {
				var xmlhttp=new XMLHttpRequest();
				xmlhttp.onreadystatechange=function() {
					if (xmlhttp.readyState==4 && xmlhttp.status==200) {
						document.getElementById("txtHint").innerHTML=xmlhttp.responseText;
					}
				}
				xmlhttp.open("GET","./dbtest.php?q="+str,true);
				xmlhttp.send();
			}    
		}
		</script>	
</head>
<body>

<?PHP

@session_start();
$sessionID = session_id();

@include("./../config/db_connect.php");
@include("./../functions/is_logged_in_for_chat.php");

$uID = $_SESSION["uID"];
$username = $_SESSION["username"];
$firstname = $_SESSION["firstname"];
$admin = $_SESSION["admin"];

$remoteADDR = $_SERVER["REMOTE_ADDR"];

$is_logged_in = is_logged_in($pdo, $sessionID);

if(($is_logged_in == 1) && ($admin == 1)){
	echo "<div id='form'>";
	echo "<span>Hallo $firstname, <a href='./logout.php'>logout</a></span><hr/>";

	echo "<h1>Benutzereinstellungen festlegen...</h1>";

?>	
	
	<p><b>Start typing a name in the input field below:</b></p>
		<form action=''>
			First name: <input type='text' id='txt1' onkeyup='showHint(this.value)'>
		</form>
	<p>Suggestions: <span id='txtHint'></span></p>

<?PHP	

} else{
	@Header("Location: ./../login.php");
}

?>

</body>
</html>