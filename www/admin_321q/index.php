<!DOCTYPE HTML>
<html lang="de-DE">
<head>
	<meta charset="UTF-8">
	<title>Adminbereich</title>

<link rel="stylesheet" type="text/css" href="./../css/admin/index.css">
	
</head>
<body>

<?PHP

@session_start();

@include("./../config/db_connect.php");
@include("./../functions/is_logged_in.php");
@include("./../functions/admin_registrants.php");

$uID = $_SESSION["uID"];
$username = $_SESSION["username"];
$firstname = $_SESSION["firstname"];
$admin = $_SESSION["admin"];

$remoteADDR = $_SERVER["REMOTE_ADDR"];

$is_logged_in = is_logged_in($pdo, $uID, $remoteADDR);

if(($is_logged_in == 1) && ($admin == 1)){
	echo "<div id='form'>";
	echo "<span>Hallo $firstname, <a href='./../intern/logout.php'>logout</a></span><hr/>";
	echo "<span><a href='./../intern/index2.php'>zurück zum Benutzerbereich!</a><br><br/></span>";
	
	if($admin == 1){
		
		###############################
		# hier anzeige rein von neuen anmeldungen...
		
		$registrants = get_registrants($pdo);
				
		if($registrants["total"] > 0){
			echo "<form action='./admin_proceed.php' method='POST'>";
			
			echo "<table border='0' width='900px'>";
			echo "  <tr>";
			echo "    <td>&nbsp;</td>";
			echo "    <td><b>Benutzername</b></td>";
			echo "    <td><b>Name</b></td>";
			echo "    <td><b>E-Mail</b></td>";
			echo "    <td><b>Anmeldedatum</b></td>";
			echo "  </tr>";
			
			echo "  <tr>";
			echo "    <td colspan='10'>&nbsp;</td>";
			echo "  </tr>";
			
			for($i = 0; $i < $registrants["total"]; $i++){
				$registerDATE = date("d.m.Y H:i", $registrants[$i]["regdate"]);

				switch($i % 2){
					case 0: 
						echo "<tr bgcolor='#FFFFFF'>";
						break; 
										
					case 1: 
						echo "<tr bgcolor='#AFAFAF'>";
						break; 
				}
						
				echo "  <td><input type='checkbox' name='registrants[$i]' value='" . $registrants[$i]["regID"] . "'></td>";
				echo "  <td>" . $registrants[$i]["username"] . "</td>";
				echo "  <td>" . $registrants[$i]["firstname"] . " " . $registrants[$i]["lastname"] . "</td>";
				echo "  <td>" . $registrants[$i]["email"] . "</td>";
				echo "  <td>" . $registerDATE . "</td>";				
				echo "</tr>";
			}
			
			echo "  <tr>";
			echo "    <td colspan='10'>&nbsp;</td>";
			echo "  </tr>";			
			
			echo "  <tr>";
			echo "    <td colspan='10'>";
			
			echo "<select name='action'>";
			echo "  <option value='1'>aufnehmen";
			echo "  <option value='0'>ablehnen";
			echo "</select>&nbsp;&nbsp;&nbsp;<input type='submit' value=' speichern '>";
			
			echo "    </td>";
			echo "  </tr>";
			
			echo "</table>";
			
			echo "<input type='hidden' name='total' value='" . $registrants["total"] . "'>";
			
			echo "</form>";
		} else{
			echo "<div id='nocontent'>Schade, es sind keine neuen Anmeldungen vorhanden...</div>";
		}
	}
	
	echo "</div>";
} else{
	@Header("Location: ./../login.php");
}

?>

</body>
</html>