<?PHP
session_start();
$sessionID = session_id();

@include("./../config/db_connect.php");
@include("./../functions/is_logged_in.php");

$uID = $_SESSION["uID"];
$admin = $_SESSION["admin"];

$remoteADDR = $_SERVER["REMOTE_ADDR"];

$is_logged_in = is_logged_in($pdo, $uID, $remoteADDR);

if(($is_logged_in == 1) && ($admin == 1)){

	$registrants = $_POST["registrants"];
	$action = $_POST["action"];
	$total = $_POST["total"];

	for($i = 0; $i < $total; $i++){
		
		if(!@empty($registrants[$i])){
		
			switch($action){
				// decline
				case 0: {
					$bp_status = 0;
			
					$stmt0 = $pdo->prepare("UPDATE doit_register SET `status` = :bp_status WHERE `regID` = :bp_regID");
					$stmt0->bindParam(':bp_regID', $registrants[$i]);
					$stmt0->bindParam(':bp_status', $bp_status);
	
					$result0 = $stmt0->execute();

					break;
				}
				
				// accept
				case 1: {
					$bp_status = 1;
			
					$stmt1 = $pdo->prepare("UPDATE doit_register SET `status` = :bp_status WHERE `regID` = :bp_regID");
					$stmt1->bindParam(':bp_regID', $registrants[$i]);
					$stmt1->bindParam(':bp_status', $bp_status);
					
					$result1 = $stmt1->execute();
			
					$stmt2 = $pdo->prepare("SELECT * FROM doit_register WHERE `regID` = :bp_regID");
					$stmt2->bindParam(':bp_regID', $registrants[$i]);
									
					$result2 = $stmt2->execute();
				
					$result2 = $stmt2->fetch(PDO::FETCH_OBJ);
				
					$username = $result2->username;
					$firstname = $result2->firstname;
					$lastname = $result2->lastname;
					$email = $result2->email;
					$password = $result2->password;
					$regdate = $result2->regdate;

					$stmt3 = $pdo->prepare("INSERT INTO doit_users SET `regID` = :bp_regID, `username` = :bp_username, `firstname` = :bp_firstname, `lastname` = :bp_lastname, `email` = :bp_email, `password` = :bp_password, `regdate` = :bp_regdate, `status` = 1");
					$stmt3->bindParam(':bp_regID', $registrants[$i]);
					$stmt3->bindParam(':bp_username', $username);
					$stmt3->bindParam(':bp_firstname', $firstname);
					$stmt3->bindParam(':bp_lastname', $lastname);
					$stmt3->bindParam(':bp_email', $email);
					$stmt3->bindParam(':bp_password', $password);
					$stmt3->bindParam(':bp_regdate', $regdate);
									
					$result3 = $stmt3->execute();

					$bp_status2 = 2;
			
					$stmt4 = $pdo->prepare("UPDATE doit_register SET `status` = :bp_status2 WHERE `regID` = :bp_regID");
					$stmt4->bindParam(':bp_regID', $registrants[$i]);
					$stmt4->bindParam(':bp_status2', $bp_status2);
									
					$result4 = $stmt4->execute();

					break;
				}
			}
		}
	}
}

@Header("Location: ./index.php");

?>