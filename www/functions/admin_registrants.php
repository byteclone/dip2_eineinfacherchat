<?PHP

function get_registrants($pdo){

	$content = array(array());

	$bp_status = 1;

	$stmt1 = $pdo->prepare("SELECT * FROM doit_register WHERE `status` = :bp_status");
	$stmt1->bindParam(':bp_status', $bp_status);
		
	$result1 = $stmt1->execute();
	$ergs = $stmt1->rowCount();
	
	for($i = 0; $i < $ergs; $i++){
		$result2 = $stmt1->fetch(PDO::FETCH_OBJ);

		$content[$i]["regID"] = $result2->regID;
		$content[$i]["username"] = $result2->username;
		$content[$i]["firstname"] = $result2->firstname;
		$content[$i]["lastname"] = $result2->lastname;
		$content[$i]["email"] = $result2->email;
		$content[$i]["regdate"] = $result2->regdate;
	}
	
	$content["total"] = $ergs;
	
	return $content;
}

?>