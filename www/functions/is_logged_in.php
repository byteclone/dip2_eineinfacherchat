<?PHP

function is_logged_in($pdo, $uID, $remoteADDR){

    $login = 0;

	$stmt1 = $pdo->prepare("SELECT * FROM doit_sessions WHERE `uID` = :bp_uID AND `remoteADDR` = :bp_remoteADDR");
	$stmt1->bindParam(':bp_uID', $uID);
	$stmt1->bindParam(':bp_remoteADDR', $remoteADDR);
	
	$result1 = $stmt1->execute();
	$login = $stmt1->rowCount(); // wurde ein Datensatz mit Bedingung uID und remoteADDR gefunden?
 	
	return $login;
}

?>