<?PHP

// Ist der Benutzername bereits in der Datenbank vorhanden?
function username_exists($pdo, $username){
	
	$ergebnis = 0;
	
	// Abfrage ob Benutzername bereits existent!
	$stmt1 = $pdo->prepare("SELECT * FROM doit_users WHERE `username` = :bp_username");
	$stmt1->bindParam(':bp_username', $username);
		
	$result1 = $stmt1->execute();
	$erg1 = $stmt1->rowCount(); // wurde ein Datensatz mit Bedingung uID und remoteADDR gefunden?

	return $ergebnis;
}

// Ist die E-Mail bereits in der Datenbank vorhanden?
function email_exists($pdo, $email){
	
	$ergebnis = 0;
	
	$stmt1 = $pdo->prepare("SELECT * FROM doit_register WHERE `email` = :bp_email");
	$stmt1->bindParam(':bp_email', $email);
		
	$result1 = $stmt1->execute();
	$erg1 = $stmt1->rowCount(); // wurde ein Datensatz mit Bedingung uID und remoteADDR gefunden?
    	
	$stmt2 = $pdo->prepare("SELECT * FROM doit_users WHERE `email` = :bp_email");
	$stmt2->bindParam(':bp_email', $email);
		
	$result2 = $stmt2->execute();
	$erg2 = $stmt2->rowCount(); // wurde ein Datensatz mit Bedingung uID und remoteADDR gefunden?
    
	// wurde aus anmelde_db oder aus register_db Datensatz mit eingegebener E-Mail Adresse gefunden?
	if(($erg1 == 1) || ($erg2 == 1)){
		$ergebnis = 1;
	}
	
	return $ergebnis;
}

?>