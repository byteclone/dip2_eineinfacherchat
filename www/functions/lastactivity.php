<?PHP

function lastactivity($pdo, $uID){

    $timenow = time();

	$timeleft = $timenow - 60 * 5;
	
	$stmt1 = $pdo->prepare("SELECT * FROM doit_chat WHERE `uID` = :bp_uID AND `timestamp` >= :bp_timenow AND `timestamp` <= :bp_timeleft");
	$stmt1->bindParam(':bp_uID', $uID);
	$stmt1->bindParam(':bp_timenow', $timenow);
	$stmt1->bindParam(':bp_timeleft', $timeleft);
	$result1 = $stmt1->execute();
	$count = $stmt1->rowCount();
	
	// Logge den Benutzer automatisch nach erreichen der Inaktivitätszeit von 5 Minuten aus...
	if($count > 0){
		$stmt2 = $pdo->prepare("DELETE FROM doit_sessions WHERE `uID` <= :bp_uID");
		$stmt2->bindParam(':bp_uID', $uID);
		$result2 = $stmt2->execute();
	}	
}

?>