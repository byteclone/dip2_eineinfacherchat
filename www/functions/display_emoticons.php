<?PHP
	 
// emoticons, nicht copy und paste von wikipedia oder sonst wo
// lösung: manuell eingeben..., dann klappts auch
// ich hatte von wikipedia per copy und paste die emoticon kürzel automatisch in die datenbank übernommen

// lädt die vollständige liste an einbindbaren emoticons und ersetzt die shorttags im text mit den image dateien...
function display_emoticons($pdo){
		
  $emoticonlist = "";
		
  $stmt1 = $pdo->prepare("SELECT shorttag, image, credits FROM doit_emoticons");
  $result1 = $stmt1->execute();	
  $ergs1 = $stmt1->rowCount();
    
  for($i = 0; $i < $ergs1; $i++){  
    $result1 = $stmt1->fetch(PDO::FETCH_OBJ);
    $shorttag = $result1->shorttag;
	$image = $result1->image;
	$credits = $result1->credits;
		
	$emoticonlist .= "<img src='" . $image . "' alt='" . $credits . "'> " . $shorttag . "<br/>";
  }
  
  return $emoticonlist;
}

?>