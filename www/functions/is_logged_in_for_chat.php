<?PHP

function is_logged_in($pdo, $sessionID){

    $login = 0;

	$stmt1 = $pdo->prepare("SELECT * FROM doit_sessions WHERE `sessionID` = :bp_sessionID");
	$stmt1->bindParam(':bp_sessionID', $sessionID);
	
	$result1 = $stmt1->execute();
	$login = $stmt1->rowCount(); // wurde ein Datensatz mit Bedingung uID und remoteADDR gefunden?
 	
	return $login;
}

?>