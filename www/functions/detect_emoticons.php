<?PHP

// lädt die vollständige liste an einbindbaren emoticons und ersetzt die shorttags im text mit den image dateien...
function detect_emoticons($pdo, $message){
		
  $stmt1 = $pdo->prepare("SELECT shorttag, image, credits FROM doit_emoticons");
  $result1 = $stmt1->execute();	
  $ergs1 = $stmt1->rowCount();
    
  for($i = 0; $i < $ergs1; $i++){  
    $result1 = $stmt1->fetch(PDO::FETCH_OBJ);
    $shorttag = $result1->shorttag;
	$image = $result1->image;
	$credits = $result1->credits;
		
	$image_new = "<img src='" . $image . "' alt='" . $credits . "'>";
	$message = str_replace($shorttag, $image_new, $message);
  }
  
  return $message;
}

?>