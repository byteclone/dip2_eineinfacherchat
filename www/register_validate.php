<?PHP
// Sitzung starten...
session_start();

// Einbinden der DB-Verbindungs Aufbau Datei
@include("./config/db_connect.php");
@include("./functions/register.php");

// hole abgesendete Daten per $_POST
$username = strip_tags(htmlspecialchars(htmlentities($_POST["username"])));
$firstname = strip_tags(htmlspecialchars(htmlentities($_POST["firstname"])));
$lastname = strip_tags(htmlspecialchars(htmlentities($_POST["lastname"])));
$email = strip_tags(htmlspecialchars(htmlentities($_POST["email"])));
$email_repeat = strip_tags(htmlspecialchars(htmlentities($_POST["email_repeat"])));
$password = strip_tags(htmlspecialchars(($_POST["password"])));
$password_repeat = strip_tags(htmlspecialchars($_POST["password_repeat"]));

// Ermittle die L�nge
$password_length = strlen($password);
$username_length = strlen($username);
$firstname_length = strlen($firstname);
$lastname_length = strlen($lastname);
$email_length = strlen($email);
$password_length = strlen($password);

// Initialisiere failure, content und errmsg Array
$failure_array = array();
$content_array = array();
$errmsg_array = array();

// Daten in session Variable content_array speichern..., zwecks Anzeige im Fehlerfall
// w�re es hier nicht angebrachter direkt $_POST zu verwenden?
$content_array["username"] = $username;
$content_array["firstname"] = $firstname;
$content_array["lastname"] = $lastname;
$content_array["email"] = $email;
$content_array["email_repeat"] = $email_repeat;

// Ist ein Account mit dem angegebenen Benutzernamen bereits existent?
$username_exists = username_exists($pdo, $username);

// wenn nein
if($username_exists == 0){
	// Ist Benutzername mindestens 4 Zeichen lang?
	if($username_length <= 3){
		$failure_array[0] = 1;
		$errmsg_array[0] = "Benutzername zu kurz. Mindestens 4 Zeichen!";
	}
} else{
	// Ist Benutzername bereits existent!
	if($username_exists == 1){
		$failure_array[0] = 1;
		$errmsg_array[0] = "Benutzername bereits existent!";
	}
}

// Ist Vorname mindestens 3 Zeichen lang?
if($firstname_length < 3){
  $failure_array[1] = 1;
  $errmsg_array[1] = "Vorname zu kurz. Mindestens 3 Buchstaben!";
}

// https://stackoverflow.com/questions/10752862/password-strength-check-in-php
// used the solution in first answer
if(preg_match("#[0-9]+#", $firstname)) {
    $failure_array[11] = 1;
	$errmsg_array[11] = "Vorname darf keine Zahlen enthalten...";
}

// Ist Name mindestens 3 Zeichen lang?
if($lastname_length < 3){
  $failure_array[2] = 1;
  $errmsg_array[2] = "Name zu kurz. Mindestens 3 Buchstaben!";
}

// https://stackoverflow.com/questions/10752862/password-strength-check-in-php
// used the solution in first answer
if(preg_match("#[0-9]+#", $lastname)) {
    $failure_array[12] = 1;
	$errmsg_array[12] = "Nachname darf keine Zahlen enthalten...";
}

// Entspricht die E-Mail der E-Mail Wiederholung?
if($email != $email_repeat){
  $failure_array[3] = 1;
  $errmsg_array[3] = "E-Mails stimmen nicht �berein!";
}

// Befindet sich �berhaupt etwas in E-Mail?
if(@empty($email)){
  $failure_array[4] = 1;
  $errmsg_array[4] = "Bitte geben Sie eine E-Mail Adresse an.";
}

// Ist E-Mail im System bereits existent oder nicht?
$email_exists = email_exists($pdo, $email);

if($email_exists == 1){
  $failure_array[5] = 1;
  $errmsg_array[5] = "Diese E-Mail Adresse wird bereits verwendet";
}

#####################################################################################
# passwort �berpr�fen ab hier

// Entspricht Password der Password Wiederlung?
if($password != $password_repeat){
	$failure_array[6] = 1;
	$errmsg_array[6] = "Passw�rter stimmen nicht �berein!";
}

// Ist das Password mindestens 5 Zeichen lang?
if($password_length  < 5){
	$failure_array[7] = 1;
	$errmsg_array[7] = "Passwort zu kurz, muss mindestens 5 Zeichen enthalten!";
}
	
// https://stackoverflow.com/questions/10752862/password-strength-check-in-php
// used the solution in first answer
if(!preg_match("#[0-9]+#", $password)) {
       $failure_array[8] = 1;
	$errmsg_array[8] = "Passwort sollte mindestens eine Nummer enthalten!";
}
	
// https://stackoverflow.com/questions/10752862/password-strength-check-in-php
// used the solution in first answer
if(!preg_match("#[a-zA-Z]+#", $password)) {
    $failure_array[9] = 1;
       $errmsg_array[9] = "Passwort sollte mindestens einen Buchstaben enthalten!";
}   

#
#####################################################################################

$email_correct = filter_var($email, FILTER_VALIDATE_EMAIL);

if($email_correct == 1){
  $failure_array[10] = 1;
  $errmsg_array[10] = "Dies ist keine g�ltige E-Mail Adresse!";
}


// Pr�fe ob irgend wo im failure_array eine 1 vorkommt! 1 = in diesem Falle bedeutet Fehler vorhanden
$failure_exists = in_array(1, $failure_array);

// wenn Fehler existiert, speicher Daten in Session und leide den Benutzer zum Registrierungsformular zur�ck
if($failure_exists == 1){
  $_SESSION["failure_array"] = $failure_array;
  $_SESSION["content_array"] = $content_array;
  $_SESSION["errmsg_array"] = $errmsg_array;
  
  @Header("Location: ./register.php");
  die();
} elseif($failure_exists == 0){ // wenn kein Fehler vorhanden ist, dann speicher die Daten des Anwenders in der Datenbank
    $regdate = time();

	$pw_sha1 = SHA1($password); 

	$stmt1 = $pdo->prepare("INSERT INTO doit_register SET `username` = :bp_username, `firstname` = :bp_firstname, `lastname` = :bp_lastname, `email` = :bp_email, `password` = :bp_password, `regdate` = :bp_regdate, `status` = '1'");
	$stmt1->bindParam(':bp_username', $username);
	$stmt1->bindParam(':bp_firstname', $firstname);
	$stmt1->bindParam(':bp_lastname', $lastname);
	$stmt1->bindParam(':bp_email', $email);
	$stmt1->bindParam(':bp_password', $pw_sha1);	
	$stmt1->bindParam(':bp_regdate', $regdate);
	
	$result1 = $stmt1->execute();
	
	$lastID = $pdo->lastInsertId();  // http://php.net/manual/de/pdo.lastinsertid.php

	if(!@empty($lastID)){	
		@Header("Location: ./success.html");
	} else{
		@Header("Location: ./failure.html");
	}
} else{
  @Header("Location: ./failure.html");
}

?>