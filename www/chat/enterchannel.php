<?PHP
session_start();
$sessionID = session_id();

echo "" . $sessionID . "<br/>";
?>

<!DOCTYPE HTML>
<html lang='de-DE'>
<head>
	<meta charset='UTF-8'>
	<title>Login</title>
	
	<link rel="stylesheet" type="text/css" href="./../css/login_register.css">
	<link rel="stylesheet" type="text/css" href="./../css/login.css">

	<script>
		function showHint(str)
		{
			if (str.length==0) { 
				document.getElementById("txtHint").innerHTML="";
				return;
			} else {
				var xmlhttp=new XMLHttpRequest();
				xmlhttp.onreadystatechange=function() {
					if (xmlhttp.readyState==4 && xmlhttp.status==200) {
						document.getElementById("txtHint").innerHTML=xmlhttp.responseText;
					}
				}
				xmlhttp.open("GET","./dbtest.php?q="+str,true);
				xmlhttp.send();
			}    
		}
		</script>	
</head>
<body>
	
<?PHP	
@include("./../config/db_connect.php");
@include("./../functions/is_logged_in_for_chat.php");

$uID = $_SESSION["uID"];

echo "<hr/>" . $uID . "<hr/>";
	
$is_logged_in = 0;

if(!@empty($uID)){
	$is_logged_in = is_logged_in($pdo, $sessionID);
} 

	
if($is_logged_in == 1){	
	
	echo "<form action='./login_proceed.php' method='POST'>";
		
	echo "	<div id='form' class='form_height'>";
	echo "		<div id='header'>Enter Chatroom</div>";
	echo "		<div id='spacer'><hr></div>";
	echo "			<div><label for='email'>Chatroom</label><br/><input type='text' name='crNAME' id='txt1' onkeyup='showHint(this.value)' size='38' maxlength='75'></div>";
    echo "        	<div id='spacer'><hr></div>";
	echo "		<div id='footer_wrapper'>";
	echo "<p>Vorschl&auml;ge: <span id='txtHint'></span></p>";
	echo "		</div>";
	echo "	</div>";
	
	echo "</form>";

}
	
?>
	
</body>
</html>