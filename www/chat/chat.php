﻿<?PHP
session_start();
$sessionID = session_id();
include("./../config/db_connect.php");
include("./../functions/is_logged_in_for_chat.php");
include("./../functions/detect_emoticons.php");
include("./../functions/display_emoticons.php");
include("./../functions/blacklist.php");
// include("./../css/chat/style.php");

# include("./getmessages.php");
# include("./whoisonline.php");

$is_online = is_logged_in($pdo, $sessionID);

if($is_online == 0){
  @Header("Location: ./pleaselogin.html");
}

$uID = $_SESSION["uID"];

?>

<html>
<head>
<title>Ein einfacher Chat</title>

<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>

<link rel='stylesheet' type='text/css' href='./../css/chat/style.php' />

</head>

<body>

<form action="./savemessage.php" method="POST" id="text-form">
<div id="wrapper">
  <div id="wrapper_left">
    
	<div id="chatwindow">
        
		<?PHP 
		  // https://www.lima-city.de/thread/werte-in-echtzeit-aus-datenbank-ausgeben-ajax
		  
		  $loop = $_SESSION["loop"];
		  
		  // weil wenn das nicht gesetzt ist kann es sein, das er zweimal den loop macht und es dann entsprechend ausgibt!
		  if($loop == 1){
		    include('getmessages.php'); 
		  }
		  
		  unset($_SESSION["loop"]);
		?>
			
		<script>
        setInterval(function(){
            $.get('getmessages.php', function(data) {
                $('#chatwindow').html(data);
            });
        }, 5000);
		</script>
		
    </div>
    <div id="textmessage">&nbsp;Ihre Textnachricht:&nbsp;&nbsp;<input type="text" name="chatmessage"></div>
    <div id="spacer">&nbsp;</div>
  </div>

  <div id="wrapper_right">
    <div id="logout"><a href="./../intern/logout.php">Logout</a></div>
	<div id="spacer">&nbsp;</div>
    <div id="whoisonline">
		        
		<?PHP 
		  // https://www.lima-city.de/thread/werte-in-echtzeit-aus-datenbank-ausgeben-ajax
		  include('whoisonline.php'); 
		?>		
		
        <script>
        setInterval(function(){
            $.get('whoisonline.php', function(data) {
                $('#reloaded').html(data);
            });
        }, 10000);
		</script>
		
	</div>
	<div id="spacer">&nbsp;</div>
	<div id="emoticonbox">
	  <div id="reloaded_header">Funktionierende Smileys</div>
	
	<?PHP
		 
	$emoticons = display_emoticons($pdo);
	echo "" . $emoticons . "";
	
	?>
	
	  </div>
	<div id="spacer">&nbsp;</div>	
    <div id="button_save"><input type="submit" value=" enter drücken statt button "></div>	
  
  </div>
</div>
</form>

<script>
// https://api.jquery.com/jquery.post/
// https://api.jquery.com/focus/

	$('#text-form').submit(function(event) {
	
		$.ajax({
		  type: "POST",
		  url: "./savemessage.php",
		  data: {
			chatmessage: $('input[name=chatmessage]').val()
		  },
		  success: function() {
			 $('input[name=chatmessage]').val("").focus();
		  }
		});
	
		return false;
	});

</script>

</body>
</html>