<?PHP

if(!isset($sessionID)){
  @session_start();
  $sessionID = session_id();
  @include("./../config/db_connect.php");
  include("./../functions/is_logged_in_for_chat.php");
  include("./../functions/detect_emoticons.php");
  include("./../functions/display_emoticons.php");
  include("./../functions/blacklist.php");
  $is_online = is_logged_in($pdo, $sessionID);
}

// $chatmessage = @$_POST["chatmessage"];

$uID = $_SESSION["uID"];

// hole den zeitstempel des logins vom aktuellen benutzer...
$stmt0 = $pdo->prepare("SELECT crID, timestamp FROM doit_sessions WHERE `sessionID` = :bp_sessionID");
$stmt0->bindParam(':bp_sessionID', $sessionID);
$result0 = $stmt0->execute();

$result0 = $stmt0->fetch(PDO::FETCH_OBJ);
$crID = $result0->crID;
$timestamp0 = $result0->timestamp;

// var_dump($timestamp0);

// aktuelle uhrzeit
$timenow = time();
	
// hole die nachrichten anhand der zeitstempel...
$stmt1 = $pdo->prepare("SELECT uID, message, timestamp FROM doit_chat WHERE `crID` = :bp_crID AND `timestamp` >= :bp_timestamp AND `timestamp` <= :bp_timenow ORDER BY `timestamp` DESC");
$stmt1->bindParam(':bp_crID', $crID);
$stmt1->bindParam(':bp_timestamp', $timestamp0);
$stmt1->bindParam(':bp_timenow', $timenow);
$result1 = $stmt1->execute();
$ergs1 = $stmt1->rowCount();

// $msg = "<div id='reloaded2'>";

// $msg = "";

// hole den zeitstempel des logins vom aktuellen benutzer...

for($i = 0; $i < $ergs1; $i++){
  	$result1 = $stmt1->fetch(PDO::FETCH_OBJ);
	$wID = $result1->uID; // benenne userID in writerID um
	$crID = $result1->crID;
	$message = $result1->message;
	$timestamp = $result1->timestamp;
	
	$datetime = date("d.m.Y H:i", $timestamp);
		
    $stmt2 = $pdo->prepare("SELECT username FROM doit_users WHERE `uID` = :bp_wID");
    $stmt2->bindParam(':bp_wID', $wID);
    $result2 = $stmt2->execute();	
	
	$result2 = $stmt2->fetch(PDO::FETCH_OBJ);
	$username = $result2->username;
	
	$message = detect_emoticons($pdo, $message);
	$on_blacklist = blacklist($pdo, $uID, $wID);
	
	// hier liegt der hund irgend wo begraben da er immer das erste oder gar alle entfernt, jenachdem wo man das chatwindow2 einfügt...	
    // $msg .= "<div id='chatwindow2'>";
	// er ignoriert vollständig das div mit der id uname...
	$msg .= "  <p>" . $username . " schrieb am " . $datetime . " Uhr</p>";
	
	if($on_blacklist == 0){
		$msg .= "  <div id='chatwindow_message'>" . $message . "</div>"; // <h1>" . $ergs1 . " | " . $i . "</h1></div>";
	} else{
		$msg .= "  <div id='chatwindow_message'><mark>Benutzer steht auf deiner Blacklist!</mark></div>"; // <h1>" . $ergs1 . " | " . $i . "</h1></div>";
	}
    // $msg .= "</div>";
	$msg .= "<div id='spacer'>&nbsp;</div>";
}

// $msg .= "</div>";
// $msg .= "</div>";

// sicherheitslücke schließen nach dem fehler den herr nowak gefunden hatte...
if($is_online == 1){
  echo $msg;
  $_SESSION["loop"] = 1;
}

/*
		  
	<p>Verfasser 1 schrieb am 13.02.2018 um 10:33 Uhr</p>
	<div id="chatwindow_message">
	Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.
	</div>
  
*/

?>