<?PHP
session_start();
$sessionID = session_id();
include("./../config/db_connect.php");
include("./../functions/is_logged_in_for_chat.php");

// include("./../css/chat/style.php");

# include("./getmessages.php");
# include("./whoisonline.php");

$is_online = is_logged_in($pdo, $sessionID);

if($is_online == 0){
  @Header("Location: ./pleaselogin.html");
} elseif($is_online == 1){
	$uID = $_SESSION["uID"];
	$crID = $_POST["crID"];
	$crPASSWORD = $_POST["password"];

	// echo "" . $sessionID . "<br/>";
	// echo "" . $uID . "<br/>";
	// echo "" . $crID . "<br/>";
	// echo "" . $crPASSWORD . "<br/>";
	
	$pw_sha1 = SHA1($crPASSWORD);
	
	// echo "" . $pw_sha1 . "";
	
	$stmt1 = $pdo->prepare("SELECT * FROM doit_chatrooms WHERE `crID` = :bp_crID AND `crPASSWORD` = :bp_crPASSWORD");
	$stmt1->bindParam(':bp_crID', $crID);
	$stmt1->bindParam(':bp_crPASSWORD', $pw_sha1);
	$result1 = $stmt1->execute();
	$ergs1 = $stmt1->rowCount();
	
	// echo "<br/>???" . $ergs1 . "";
	
	if($ergs1 == 1){
		// echo "<br/>hu?";
	
		$stmt2 = $pdo->prepare("UPDATE doit_sessions SET `crID` = :bp_crID WHERE `uID` = :bp_uID");
		$stmt2->bindParam(':bp_uID', $uID);
		$stmt2->bindParam(':bp_crID', $crID);
		$result2 = $stmt2->execute();
		$ergs2 = $stmt2->rowCount();
	}
	
	@Header("Location: ./chat.php");
}

?>