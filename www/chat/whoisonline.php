<?PHP
@session_start();
$sessionID = session_id();

@include("./../config/db_connect.php");

$stmt1 = $pdo->prepare("SELECT crID FROM doit_sessions WHERE `sessionID` = :bp_sessionID");
$stmt1->bindParam(':bp_sessionID', $sessionID);
$result1 = $stmt1->execute();
$ergs1 = $stmt1->rowCount();

$result1 = $stmt1->fetch(PDO::FETCH_OBJ);
$crID = $result1->crID;

$stmt2 = $pdo->prepare("SELECT uID, sessionID FROM doit_sessions WHERE `crID` = :bp_crID");
$stmt2->bindParam(':bp_crID', $crID);
$result2 = $stmt2->execute();
$ergs2 = $stmt2->rowCount();

$ergs1_minus = $ergs2 - 1;

$stmt3 = $pdo->prepare("SELECT crNAME FROM doit_chatrooms WHERE `crID` = :bp_crID");
$stmt3->bindParam(':bp_crID', $crID);
$result3 = $stmt3->execute();
$ergs3 = $stmt3->rowCount();
 
$result3 = $stmt3->fetch(PDO::FETCH_OBJ);
$crNAME = $result3->crNAME;

$hint = "<div id='reloaded'>";

$hint .= "  <div id='reloaded_header'>Wer ist online?</div>";
$hint .= "  <div id='reloaded_header'>CHATROOM: " . $crNAME . "</div>";
$hint .= "  <div id='reloaded_spacer'>&nbsp;</div>";

for($i = 0; $i < $ergs2; $i++){
  	$result2 = $stmt2->fetch(PDO::FETCH_OBJ);
	$uID = $result2->uID;
	$sessID = $result2->sessionID;
	
    $stmt4 = $pdo->prepare("SELECT username FROM doit_users WHERE `uID` = :bp_uID");
    $stmt4->bindParam(':bp_uID', $uID);
    $result4 = $stmt4->execute();
    $ergs4 = $stmt4->rowCount();
	
	$result4 = $stmt4->fetch(PDO::FETCH_OBJ);	
	$username = $result4->username;
	
	// $hint .= $username . ",&nbsp;&nbsp;&nbsp;";
	
	  $hint .= "<div id='whoisonline_user'>";
	  $hint .= "  <div id='whoisonline_user_iswriting'>&nbsp;</div>";
	  $hint .= "  <div id='whoisinline_details'>&nbsp;&nbsp;&nbsp;" . $username . "&nbsp;&nbsp;&nbsp;";

	  if($session_id == $sessID){
	    $hint .= "me!";
	  }

	  $hint .= "  </div>";
	  $hint .= "</div>";
	  $hint .= "<div id='whoisonline_spacer'>&nbsp;</div>";
}

$hint .= "</div>";

echo $hint;

?>