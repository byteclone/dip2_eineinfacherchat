<?PHP
@session_start();
$session_id = session_id();

@include("./../config/db_connect.php");

$chatmessage = htmlentities(strip_tags(@$_POST["chatmessage"]));

$stmt1 = $pdo->prepare("SELECT uID, crID FROM doit_sessions WHERE `sessionID` = :bp_sessionID");
$stmt1->bindParam(':bp_sessionID', $session_id);
$result1 = $stmt1->execute();
$ergs1 = $stmt1->rowCount();

$ergs1_minus = $ergs1 - 1;

// for($i = 0; $i < $ergs1; $i++){
  	$result1 = $stmt1->fetch(PDO::FETCH_OBJ);
	$uID = $result1->uID;
	$crID = $result1->crID;

	$timestamp = time();
	
    $stmt2 = $pdo->prepare("INSERT INTO doit_chat SET `uID` = :bp_uID, `crID` = :bp_crID, `message` = :bp_chatmessage, `timestamp` = :bp_timestamp");
    $stmt2->bindParam(':bp_uID', $uID);
	$stmt2->bindParam(':bp_crID', $crID);
    $stmt2->bindParam(':bp_chatmessage', $chatmessage);
	$stmt2->bindParam(':bp_timestamp', $timestamp);
    $result2 = $stmt2->execute();	
// }

// @Header("Location: ./chat.php");

?>