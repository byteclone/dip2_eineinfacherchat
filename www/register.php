<!DOCTYPE HTML>
<html lang="de-DE">
<head>
	<meta charset="UTF-8">
	<title>Registrieren</title>
	
	<link rel="stylesheet" type="text/css" href="./css/login_register.css">
	<link rel="stylesheet" type="text/css" href="./css/register.css">

</head>
<body>
	
	<?PHP
	session_start();
	
	@include("./config/db_connect.php");
	@include("./functions/is_logged_in.php");

	$uID = $_SESSION["uID"];
	$remoteADDR = $_SERVER["REMOTE_ADDR"];
	
	$is_logged_in = 0;
	
	if(!@empty($uID)){
		$is_logged_in = is_logged_in($pdo, $uID, $remoteADDR);
	} 
	
	if($is_logged_in == 1){	
		@Header("Location: ./intern/index2.php");
		die();
	} else{
	
		$failure_array = @$_SESSION["failure_array"];
		$content_array = @$_SESSION["content_array"];
		$errmsg_array = @$_SESSION["errmsg_array"];
	
		echo "<form action='./register_validate.php' method='POST'>";	
		echo "<div id='form' class='form_height'>";
		echo "	<div id='header'>Registrierungsformular</div>";
		echo "	<div id='spacer'><hr></div>";
		
		if(isset($failure_array)){
			if($failure_array[0] == 1) {
				echo "	<div class='field' id='failure'>";
			} else{
				echo "<div class='field'>";
			}
			
			echo "<label for='username'>Benutzername</label><br/><input type='text' name='username' size='25' maxlength='25' value='" . $content_array["username"] . "'></div>";
        } else{		
			echo "<div class='field'><label for='username'>Benutzername</label><br/><input type='text' name='username' size='25' maxlength='25'></div>";
		}
		
		if(isset($failure_array)){
			if(($failure_array[1] == 1) || ($failure_array[11] == 1)){
				echo "	<div class='field' id='failure'>";
			} else{
				echo "<div class='field'>";
			}
			
			echo "<label for='firstname'>Vorname</label><br/><input type='text' name='firstname' size='25' maxlength='50' value='" . $content_array["firstname"] . "'></div>";
		} else{
			echo "	<div class='field'><label for='firstname'>Vorname</label><br/><input type='text' name='firstname' size='25' maxlength='50'></div>";
		}
		
		if(isset($failure_array)){
			if(($failure_array[2] == 1) || ($failure_array[12] == 1)){
				echo "	<div class='field' id='failure'>";
			} else{			
				echo "<div class='field'>";
			}
			
			echo "<label for='lastname'>Nachname</label><br/><input type='text' name='lastname' size='25' maxlength='50' value='" . $content_array["lastname"] . "'></div>";
		} else{
			echo "	<div class='field'><label for='lastname'>Nachname</label><br/><input type='text' name='lastname' size='25' maxlength='50'></div>";
		}
		echo "	<div id='spacer'><hr></div>";
		
		if(isset($failure_array)){
			if(($failure_array[3] == 1) || ($failure_array[4] == 1) || ($failure_array[5] == 1) || ($failure_array[10] == 1)){
				echo "	<div class='field' id='failure'>";
			} else{
				echo "<div class='field'>";
			}
			
			echo "<label for='email'>E-Mail</label><br/><input type='email' name='email' size='38' maxlength='75' value='" . $content_array["email"] . "'></div>";
			
			if(($failure_array[3] == 1) || ($failure_array[4] == 1) || ($failure_array[5] == 1) || ($failure_array[10] == 1)){
				echo "	<div class='field' id='failure'>";
			} else{
				echo "<div class='field'>";
			}
						
			echo "<label for='email'>E-Mail wdh.</label><br/><input type='email' name='email_repeat' size='38' maxlength='75' value='" . $content_array["email_repeat"] . "'></div>";
		} else{
			echo "	<div class='field'><label for='email'>E-Mail</label><br/><input type='email' name='email' size='38' maxlength='75'></div>";
			echo "	<div class='field'><label for='email'>E-Mail wdh.</label><br/><input type='email' name='email_repeat' size='38' maxlength='75'></div>";
		}
		
		echo "	<div id='spacer'><hr></div>";
			
		if(isset($failure_array)){
			if(($failure_array[6] == 1) || ($failure_array[7] == 1) || ($failure_array[8] == 1) || ($failure_array[9] == 1)){
				echo "	<div class='field' id='failure'>";

			}
		} else{
				echo "	<div class='field'>";

		}
		
		echo "  <div class='field'><label for='password'>Passwort</label><br/><input type='password' name='password' size='25' maxlength='25'></div>";
		echo "	<div class='field'><label for='password_repeat'>Passwort wdh.</label><br/><input type='password' name='password_repeat' size='25' maxlength='25'>";
		echo "  </div>";
		
		echo "	<div id='spacer'><hr></div>";
		echo "	<div id='footer_wrapper'>";
		echo "		<div class='footer_left'><input type='submit' value=' registrieren '><br/><br/><a href='./index.html'>zurück</a></div>";
		echo "		<div class='footer_right'><input type='reset' value=' verwerfen '><br/><br/><a href='./login.php'>zum Login</a></div>";
		echo "	</div>";
		echo "</div>";
	
		if(isset($failure_array)){	
			echo "<div id='failure_info'>";
			echo "	<div id='header'>Fehler Info</div>";
			echo "	<div id='spacer'><hr></div>";		
			echo "  <div>";
			
			$count = count($failure_array);
			
			for($i = 0; $i <= 12; $i++){
				if($failure_array[$i] == 1){			
					echo "" . $errmsg_array[$i] . "<br/><br/>";
				}
			}
		
		    echo "  </div>";
			echo "</div>";
		}
		
		echo "</form>";
		
		session_unset($_SESSION["failure_array"]);
		session_unset($_SESSION["content_array"]);
		session_unset($_SESSION["errmsg_array"]);
	}

	?>

</body>
</html>