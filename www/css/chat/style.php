<?php

if(!isset($sessionID)){
	@session_start();
	
	$sessionID = session_id();
	
	$uID = $_SESSION["uID"];
}

/*
$background = "#AFAFAF";
$chatwindow = "blue";
$pcolor = "white";
$whoisonline = "lightblue";
$highlight_user = "blue";
*/

/*
$background = "";
$chatwindow = "";
$pcolor = "";
$whoisonline = "";
$highlight_user = "";

if(!@empty($_SESSION["uID"])){
	
	$stmt1 = $pdo->prepare("SELECT * FROM doit_colorsettings WHERE `uID` = :bp_uID");
	$stmt1->bindParam(':bp_uID', $uID);
	$result1 = $stmt1->execute();
	
	$result1 = $stmt1->fetch(PDO::FETCH_OBJ);
	
	$background = $result1->background;
	$chatwindow = $result1->chatwindow;
	$pcolor = $result1->pcolor;
	$whoisonline = $result1->whoisonline;
	$highlight_user = $result1->highlight_user;
		
} else{
	$background 		 "#AFAFAF";
	$chatwindow = "blue";
	$pcolor = "white";
	$whoisonline = "lightblue";
	$highlight_user = "blue";
}
*/

# */	

// https://css-tricks.com/css-variables-with-php/
header("Content-type: text/css; charset: UTF-8");

/*
if(!isset($sessionID)){
  @session_start();
  $sessionID = session_id();
  @include("./../config/db_connect.php");
  include("./../functions/is_logged_in_for_chat.php");
  include("./../functions/detect_emoticons.php");
  include("./../functions/display_emoticons.php");
  include("./../functions/blacklist.php");
  $is_online = is_logged_in($pdo, $sessionID);
}

$uID = $_SESSION["uID"];
$username = $_SESSION["username"];
$firstname = $_SESSION["firstname"];
$admin = $_SESSION["admin"];

// $remoteADDR = $_SERVER["REMOTE_ADDR"];

// $is_logged_in = is_logged_in($pdo, $sessionID);
*/

/*
$background = "#AFAFAF";
$chatwindow = "blue";
$pcolor = "white";
$whoisonline = "lightblue";
$highlight_user = "blue";
*/

// include("./colors/colors_" . $uID . ".txt");

$is_file = is_file("./colors/colors_" . $uID . ".txt");

if($is_file == 1){
	$fopen = fopen("./colors/colors_" . $uID . ".txt", "r");
} else
{
	$fopen = fopen("./colors/colors_DEFAULT.txt", "r");
}
	
	
	
$counter = 0;
	
$array = array();
	
# /*
while($fgets = fgets($fopen, 100000)){
	// echo "" . $fgets . "<br/>";
	$explode = explode("|", $fgets);		
	$array[$counter] = $explode[1];
		
	$counter++;
}
?>

<script type='text/css'>
html, body{
  background-color: <?PHP echo $array[0]; ?>;
  height: 99%;
}

#wrapper{
  position: relative;

  width: 100%;
  height: 100%;

  display: block;
	
  border: 0px solid green;
}
  
#wrapper_left{
  float: left;
  
  width: 73.5%;
  height: 100%;

  z-index: 1000;
}

#wrapper_right{
  float: right;
  display: block;	
  
  width: 20%;
  height: 100%;
  float: right;
	
  border: 0px solid black;
}
  
#chatwindow{
  background-color: <?PHP echo $array[1]; ?>;

  width: 100%;
  height: 90%;

  padding: 10px;
	
  float: left;
	
  overflow: auto;
	
  border-top-left-radius: 20px;
  border-top-right-radius: 20px;
  border-bottom-left-radius: 20px;
  border-bottom-right-radius: 20px;
}

#chatwindow2{
  font-family: Arial;
  font-size: 12pt;

  background-color: #FFFFFF;
}
  
#reloaded, #reloaded2{
  color: #000000;
  border: 0px solid black;
}
  
#reloaded_header{
  background-color: blue;
  color: white;
    
  font-family: Arial;
  font-size: 12pt;   
  
  text-align: center;
}
  
#reloaded_spacer{
  height: 2.5%;
}
  
#chatwindow_message{
  background-color: <?PHP echo $array[2]; ?>;
	
  border-top-left-radius: 10px;
  border-top-right-radius: 10px;
  border-bottom-left-radius: 10px;
  border-bottom-right-radius: 10px;

  font-family: Arial;
  font-size: 12pt;
	
  padding: 10px;
	
  text-align: justify;
}

mark{
  color: black;
  background-color: black;
}

#logout{
  border-top-left-radius: 10px;
  border-top-right-radius: 10px;
  border-bottom-left-radius: 10px;
  border-bottom-right-radius: 10px;

  border: 0px solid red;
		
  width: 100%;
  height: 5vh;
	
  line-height: 5vh;	
  text-align: center;
  
  font-family: Arial;
  font-size: 24pt;
}
  
#whoisonline{
  background-color: <?PHP echo $array[3]; ?>;

  width: 100%;
  height: 67%;
  
  border-top-left-radius: 10px;
  border-top-right-radius: 10px;
  border-bottom-left-radius: 10px;
  border-bottom-right-radius: 10px;
	
  overflow: auto;
}
  
#whoisonline_user{
  width: 100%;
  height: 5%;
	
  line-height: 26pt;
  font-family: Arial;
  font-size: 12pt;
	
  background-color: #6060FC;
  color: white;
	
  display: block;
}
  
#whoisonline_user:hover{
  background-color: blue;
		
  font-weight: bold;
  font-style: italic;

}
  
#whoisonline_user_iswriting{
  height: 100%;
  width: 5%;
	
  background-color: #AFAFAF;
	
  float: left;
	
  // background-image: url("./../images/typing.gif");
  // background-size: 100% 100%;
}
  
#whoisonline_user_details{
  height: 100%;
  width: 95%;

  float: right;
}
  
#whoisonline_spacer{
  height: 1.5%;
}
  
#textmessage{
  position: absolute;
    
  width: 75%;
  height: 5vh;
  
  z-index: 10000;

  bottom: 0px;
  
  font-family: Arial;
  font-size: 12pt;
  
  background-color: #AFAFAF;
  
  border: 1px solid black;
}

#spacer{
  height: 1.75vh;
}

.uname{
  font-size: 24pt;
  border: 10px solid black;
}
  
#highlight_writer{
  color: 0000AF;
}

#emoticonbox{
  width: 92%;
  height: 14%;
  
  padding: 10px;
  
  text-align: left;
  
  border-top-left-radius: 10px;
  border-top-right-radius: 10px;
  border-bottom-left-radius: 10px;
  border-bottom-right-radius: 10px;
	  
  background-color: lightblue;
}

input[type=text]{
  width: 84.5%;
  height: 100%;
}

input[type=submit]{
  position: absolute; 
  
  width: 20%;
  height: 5vh;
	
  bottom: 0;
}
  
a{
  color: red;

  text-decoration: none;
}

a:visited{
  color: red;
}
  
p{
  text-align: right;

  font-family: Arial;
  font-size: 8pt;
  
  font-weight: bold;
  font-style: italic;
}
  
span{
  position: absolute;
  
  width: 70%;
  
  font-family: Arial;
  font-size: 12pt;
	
  text-align: justify;
	
  left: 25px;
}
</script>