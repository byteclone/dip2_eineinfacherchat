<!DOCTYPE HTML>
<html lang='de-DE'>
<head>
	<meta charset='UTF-8'>
	<title>Login</title>
	
	<link rel="stylesheet" type="text/css" href="css/login_register.css">
	<link rel="stylesheet" type="text/css" href="css/login.css">
	
</head>
<body>
	
<?PHP
session_start();
$failure = @$_SESSION["failure"];
$email = @$_SESSION["email"];
	
@include("./config/db_connect.php");
@include("./functions/is_logged_in.php");

$uID = $_SESSION["uID"];
$remoteADDR = $_SERVER["REMOTE_ADDR"];
	
$is_logged_in = 0;

if(!@empty($uID)){
	$is_logged_in = is_logged_in($pdo, $uID, $remoteADDR);
} 

	
if($is_logged_in == 1){	
	@Header("Location: ./intern/index2.php");
	die();
} else{
		
	if(isset($failure)){	
		echo "<div id='failure_info'>$failure</div><br/>";
	}
	
	echo "<form action='./login_proceed.php' method='POST'>";
		
	echo "	<div id='form' class='form_height'>";
	echo "		<div id='header'>Loginmaske</div>";
	echo "		<div id='spacer'><hr></div>";
			
	if(isset($failure))
	{
		echo "	<div class='field' id='failure'>";
		echo "<label for='email'>E-Mail</label><br/><input type='email' name='email' size='38' maxlength='75' value='$email'></div>";
	} else{
		echo "<div class='field'>";
		echo "<label for='email'>E-Mail</label><br/><input type='email' name='email' size='38' maxlength='75'></div>";
	}		
		
	echo "		<div class='field'><label for='password'>Passwort</label><br/><input type='password' name='password' size='25' maxlength='75'></div>";
    echo "        <div id='spacer'><hr></div>";
	echo "		<div id='footer_wrapper'>";
	echo "			<div class='footer_left'><input type='submit' value=' login '><br/><br/><a href='./index.html'>zurück</a></div>";
	echo "			<div class='footer_right'><input type='reset' value=' verwerfen '><br/><br/><a href='./register.php'>zur Registration</a></div>";
	echo "		</div>";
	echo "	</div>";
	
	echo "</form>";
	
	session_unset($_SESSION["failure"]);
	session_unset($_SESSION["email"]);
}
	
?>
	
</body>
</html>