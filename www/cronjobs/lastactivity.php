﻿<?PHP
@session_start();
@include("./../config/db_connect.php");

/*
  Dieses Script müsste als Cronjob laufen!
*/

$stmt1 = $pdo->prepare("SELECT * FROM doit_sessions");
$result1 = $stmt1->execute();
$count = $stmt1->rowCount();

// echo "" . $count . "<hr>";

for($i = 0; $i < $count; $i++){
    $result1 = $stmt1->fetch(PDO::FETCH_OBJ);
    $uID = $result1->uID;
	
	lastactivity($pdo, $uID);
}

function lastactivity($pdo, $uID){

    $timenow = time();
    $maxdiff = 60 * 5;
	
	$timeleft = $timenow - $maxdiff;
	
	$stmt1 = $pdo->prepare("SELECT * FROM doit_chat WHERE `uID` = :bp_uID AND `timestamp` <= :bp_timeleft AND `timestamp` >= :bp_timenow");
	$stmt1->bindParam(':bp_uID', $uID);
	$stmt1->bindParam(':bp_timenow', $timenow);
	$stmt1->bindParam(':bp_timeleft', $timeleft);
	$result1 = $stmt1->execute();
	$count = $stmt1->rowCount();
	
	// echo "" . $count . "<br/>";
	
	// Logge den Benutzer automatisch nach erreichen der Inaktivitätszeit von 5 Minuten aus...
	if($count > 0){
		$stmt2 = $pdo->prepare("DELETE FROM doit_sessions WHERE `uID` = :bp_uID");
		$stmt2->bindParam(':bp_uID', $uID);
		$stmt2->bindParam(':bp_timenow', $timenow);
		$stmt2->bindParam(':bp_timeleft', $timeleft);
		$result2 = $stmt2->execute();
		
		// echo "<hr/>???<br/>";
		
		session_destroy();
	} else{
		$stmt3 = $pdo->prepare("SELECT * FROM doit_sessions WHERE `uID` = :bp_uID");
		$result3 = $stmt3->execute();
		$count = $stmt3->rowCount();

		$result3 = $stmt3->fetch(PDO::FETCH_OBJ);
		$uTIMESTAMP = $result3->timestamp;
		
		// echo "huch?<br/>";
		$diff = abs($uTIMESTAMP - $maxdiff);

		// echo "" . $uTIMESTAMP . " " . $timeleft . " " . $diff;
		
		if($diff >= $maxdiff){
			$stmt4 = $pdo->prepare("DELETE FROM `doit_sessions` WHERE `uID` = :bp_uID");
			$stmt4->bindParam(':bp_uID', $uID);
			$result4 = $stmt4->execute();
			
			session_destroy();
			
			// echo "hier bin ich!";
		}
	}
}

?>